#########################
CCP4i2 Task Documentation
#########################

==========================================================
Import merged data, AU Contents, alignments or coordinates
==========================================================
.. toctree::
   :maxdepth: 1

   import_merged/index
   ProvideAsuContents/index
   ProvideSequence/index
   ProvideAlignment/index
   AlternativeImportXIA2/index
   coordinate_selector/index
   splitMtz/index


======================
Integrate X-ray images
======================
.. toctree::
   :maxdepth: 1

   imosflm/index
   dui/index
   xia2_dials/index
   xia2_xds/index

=================================
X-ray data reduction and analysis
=================================
.. toctree::
   :maxdepth: 1

   aimless_pipe/index
   freerflag/index
   matthews/index
   AUSPEX/index

===================================
AlphaFold and RoseTTAFold Utilities
===================================
.. toctree::
   :maxdepth: 1

   ccp4mg_edit_model/index
   mrparse/index
   editbfac/index

====================
Experimental phasing
====================
.. toctree::
   :maxdepth: 1

   crank2/crank2
   shelx/shelx
   phaser_EP_LLG/index

==============
Bioinformatics
==============
.. toctree::
   :maxdepth: 1

   ccp4mg_edit_model/index
   ccp4mg_edit_nomrbump/index
   chainsaw/index
   sculptor/index
   clustalw/index
   mrparse/index

=====================
Molecular replacement
=====================
.. toctree::
   :maxdepth: 1

   mrbump_basic/index
   phaser_pipeline/index
   molrep_pipe/index
   csymmatch/index
   fragon/index
   morda_i2/index

====================
Density Modification
====================
.. toctree::
   :maxdepth: 1

   acorn/index
   parrot/index

===========================
Model building and graphics
===========================
.. toctree::
   :maxdepth: 1

   buccaneer_build_refine_mr/index
   arp_warp_classic/index
   modelcraft/index
   coot_rebuild/index
   coot_find_waters/index
   coot_script_lines/index
   nautilus_build_refine/index
   ccp4mg_general/index
   shelxeMR/index

==========
Refinement
==========
.. toctree::
   :maxdepth: 1

   prosmart_refmac/index
   lorestr_i2/index

=======
Ligands
=======
.. toctree::
   :maxdepth: 1

   LidiaAcedrgNew/index

======================
Valiation and analysis
======================
.. toctree::
   :maxdepth: 1

   validate_protein/index
   edstats/index
   privateer/index
   pisapipe/index

=====================
Export and Deposition
=====================
.. toctree::
   :maxdepth: 1

   PrepareDeposit/index
   mergeMtz/index

=====================
Reflection data tools
=====================
.. toctree::
   :maxdepth: 1

   pointless_reindexToMatch/index
   cphasematch/index
   cmapcoeff/index
   ctruncate/index
   chltofom/index
   freerflag/index
   matthews/index


=====================
Coordinate data tools
=====================
.. toctree::
   :maxdepth: 1

   gesamt/index
   coordinate_selector/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
