#######################################
Integrate images with DIALS - using DUI
#######################################
   The DIALS User Interface (DUI) provides a robust graphical
   interface to run and control DIALS. DIALS is a modern x-ray data
   analysis package (that can also be used from a command line) which is
   used for processing diffraction images. DIALS fully supports most
   image data formats, and will integrate and index reflections.


Input
=====

   |image1|
   
   No input information is required in the task interface to run DUI,
   although there is an option available to continue from a previous task.
   The x-ray image input is controlled through the DUI interface itself; if you
   would like to continue on from a previous DUI task in the project you can
   set this in the drop down menu by selecting one of the items listed as,
   'continue from previous dials session'.

   On termination, the output reflection data objects will be automatically
   added to the project database and listed as output.

   Further information on DIALS and DUI, is available from the DIALS web-site:

   -  `DIALS Home Page <https://dials.diamond.ac.uk/>`__.

   -  `DUI Tutorial on the DIALS Home Page <https://dials.github.io/documentation/tutorials/processing_in_detail_betalactamase_dui.html>`__.

.. |image1| image:: dui_task_1.png
