CCP4i2 Update History
=====================
..
   Please do not change the format of the next line down (second part). The CI script uses this line to extract the
   the version number, which in turn is used to create the VERSION file

Changes to CCP4i2 GUI are detailed below, as well as release details. The current version of CCP4i2 is v1.1.12

v1.1.12  (CCP4 release 7.1.017, current)
----------------------------------------
- Update to MrBump/MG interface, includes AF support.
- New model import interface for Alphafold/RosettaFold support.
- Update to MrBump interface, improved model search options.
- Tweak to shelxeMR settings, so it runs faster on the defaults.
- Bug fixes, including fix for longstanding issue with lists not updating properly & a db fix for username mismatches.
- Improvements to Refmac interface and report, including rigid body ref, occupancy ref and verdict implementation.
- Improvements to aimless pipelines, particularily for multiple data-sets.

v1.1.11  (CCP4 release 7.1.016, 10.09.21)
----------------------------------------
- Update to privateer interface and report for MKIV release.
- Update to MrBump interface, added space group option and phmmer support. Initial options for Alphafold DB support included.
- Phaser-EP report improvements (Buccaneer autobuild).
- Improved aimless pipeline and report.
- Sheetbend default parameters refined.
- Bug fixes and stability improvements.

v1.1.10  (CCP4 release 7.1.014, 22.06.21)
----------------------------------------
- Minor enhancements to PhaserEP, AceDRG and Data Reduction pipelines.
- Bugfixes and cleanup of some print statements.

v1.1.09  (CCP4 release 7.1.012, 12.03.21)
-----------------------------------------
- Updated aimless pipeline.
- MrBump Update.
- Crank2 Release 2.0.253.
- Bugfix for Phaser keys.
  
v1.1.08  (CCP4 release 7.1.011, 19.02.21)
-----------------------------------------
- Update to MRParse interface.
- Improved anomalous map treatment in coot interface scripts.
- Advanced developer options for Arcimboldo.
- Other Bug fixes and improvements.

v1.1.07  (CCP4 release 7.1.009, 04.12.20)
-----------------------------------------
- Updates to the MrBump and MrBump/ccp4mg interfaces.
- Switched to the new documentation system (offline and online).
- AMPLE gui updated (helical ensembles were added).
- Minor fix to rcsb weblink.

v1.1.06  (CCP4 release 7.1.008, 08.11.20)
-----------------------------------------
- Improvements to Sheetbend Wrapper.
- Matthews calc added to ASU.
- Bug fixes for Crank2 (v2.0.251)
- New Youtube Channel.
- Aimless pipeline updates.
- Optimisation of default Acorn settings.

v1.1.05 (CCP4 release 7.1.005, 09.10.20)
----------------------------------------
- Sheetbend included in molrep pipeline.
- Fixes for new Mac release (BigSur).
- Fixes for Crank2.

v1.1.04 (CCP4 release 7.1.004, 12.09.20)
----------------------------------------
- Included sheetbend into pipelines.
- Improvements to tutorial documents.
- Fix Mosflm report.
- Update to SIMBAD/AMPLE i2 interface.
  
v1.1.03 (CCP4 release 7.1.003, 11.08.20)
----------------------------------------
- Updated ASU task.
- Font fixes (Win).
- Comprehensive improvements to i2run.

v1.1.02 (CCP4 release 7.1.002, 03.07.20)
----------------------------------------
- Various updates and bug fixes.
- Font & Icon fixes (HD comp).

v1.1.01 (CCP4 release 7.1.001, 02.06.20)
----------------------------------------
- Various task and interface fixes.
- Python 3 compatibility improved.

v1.1.00 (Released with 7.1.000)
-------------------------------
- Base release of CCP4i2 for Series 7.1 of CCP4.
